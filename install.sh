#!/bin/sh

dnf -y install dnf-plugins-core epel-release
dnf config-manager --set-enabled PowerTools
dnf group install -y "Development Tools"
dnf install -y openblas-devel zlib-devel which git wget unzip m4 pkg-config gcc-gfortran \
  python3.8 python38-pip python38-devel \
  ocaml-extlib-devel ocaml-compiler-libs ocaml-ocamlbuild-devel \
  ocaml libffi-devel openssl-devel
curl https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh -sSf | sh
opam init -a --disable-sandboxing --comp=4.10.0
export CHECK_IF_PREINSTALLED=false
eval $(opam env) && opam install -y core_bench core alcotest ppx_jane cohttp lwt odoc torch alcotest npy
update-alternatives --set python3 `which python3.8`
python3 -m pip install -U pip
curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain nightly-2020-07-27 --no-modify-path -y
echo "source $HOME/.cargo/env" >>$HOME/.bashrc
source $HOME/.cargo/env
rustup component add rustfmt clippy

# rustup component add rls rust-analysis rust-src rustfmt
# opam install -y ocamlformat odoc
# opam pin add -y ocaml-lsp-server https://github.com/ocaml/ocaml-lsp.git
